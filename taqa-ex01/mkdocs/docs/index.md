# Logovanje zaposlenih

##### Apps
- Main app
* `Database`
- [Phone check](phonecheck.md) 
- Scan app

##### Third party apps
[ActiveMQ](https://activemq.apache.org/) should be available from outside, so [ActiveMQ](appserver.md#installation) instances can connect to it 

![Screenshot](img/appimage.png)

###### Application options 
| App name | Description |
| ------------ | ------------ |
| Phone check | Checking cell phones in network |
| Scan app | Scanning pretty faces |

[Complete config example](files/configfile.txt) 

