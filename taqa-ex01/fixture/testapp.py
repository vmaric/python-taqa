import app
import unittest

class TestApp(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.calc_object = app.Calculator()
    def test_add(self): 
        self.assertEqual(self.calc_object.add(2,3),5)
    def test_sub(self): 
        self.assertEqual(self.calc_object.sub(4,1),3)
 
unittest.main()